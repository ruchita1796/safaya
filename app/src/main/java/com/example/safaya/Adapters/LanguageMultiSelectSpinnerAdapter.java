package com.example.safaya.Adapters;

import android.content.Context;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.safaya.Fragments.RegFrag;
import com.example.safaya.Listener;
import com.example.safaya.Model.MultiSelectSpinnerModel;
import com.example.safaya.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class LanguageMultiSelectSpinnerAdapter extends ArrayAdapter<MultiSelectSpinnerModel> {

    private Context mContext;
    private ArrayList<MultiSelectSpinnerModel> listState;
    private LanguageMultiSelectSpinnerAdapter myAdapter;
    private boolean isFromView = false;
    public  ArrayList<String> selectedNameList;

    //Changed by Isha for setting SELECTED ITEMS COUNT ON MULTISELECT SPINNER
    private Listener callback;

    public LanguageMultiSelectSpinnerAdapter(Context context, int resource, List<MultiSelectSpinnerModel> objects,Listener l) {
        super(context, resource, objects);
        this.mContext = context;
        this.listState = (ArrayList<MultiSelectSpinnerModel>) objects;
        this.myAdapter = this;
      //  Changed by Isha for setting SELECTED ITEMS COUNT ON MULTISELECT SPINNER
        if (l != null) {
            callback = l;
        }
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater layoutInflator = LayoutInflater.from(mContext);
            convertView = layoutInflator.inflate(R.layout.spinner_item, null);
            holder = new ViewHolder();
            holder.mTextView = (TextView) convertView.findViewById(R.id.text);
            holder.mCheckBox = (CheckBox) convertView.findViewById(R.id.checkbox);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        selectedNameList = new ArrayList<>();

        holder.mTextView.setText(listState.get(position).getTitle());

        // To check weather checked event fire from getview() or user input
        isFromView = true;
        holder.mCheckBox.setChecked(listState.get(position).getSelected());
        isFromView = false;

        String compareLang = listState.get(position).getTitle().trim();
        String language = String.valueOf((R.string.language));

        if ((position == 0)) {
            holder.mCheckBox.setVisibility(View.INVISIBLE);
        }
      /*  else if ((position == 1) && (compareLang.equalsIgnoreCase("Language"))) {
            holder.mCheckBox.setVisibility(View.INVISIBLE);
        } */
        else {
            holder.mCheckBox.setVisibility(View.VISIBLE);
        }


        holder.mCheckBox.setTag(position);
        holder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int getPosition = (Integer) buttonView.getTag();

                //Changed by Isha for setting SELECTED ITEMS COUNT ON MULTISELECT SPINNER
                if (callback != null) {
                    View root = parent.getRootView();
//                    root.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
//                    root.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_BACK));
                    callback.getselectedCount(selectedNameList.size());
//                    callback.clicked(position);

                }


                if (!isFromView) {
                    listState.get(position).setSelected(isChecked);
                }

                if (isChecked) {
                    selectedNameList.add(listState.get(position).getTitle());
                    Toast.makeText(mContext, "Selected Items are :" + listState.get(position).getTitle(), Toast.LENGTH_LONG).show();
                } else {
                    selectedNameList.remove(listState.get(position).getTitle());
                }

            }

        });
        holder.mCheckBox.setChecked(listState.get(position).getSelected());
        return convertView;
    }

    private class ViewHolder {
        private TextView mTextView;
        private CheckBox mCheckBox;
    }

}
