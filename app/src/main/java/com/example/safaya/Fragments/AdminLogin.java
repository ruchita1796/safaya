package com.example.safaya.Fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.safaya.Api.APIClient;
import com.example.safaya.Api.APIInterface;
import com.example.safaya.Model.LoginModel;
import com.example.safaya.Model.LoginResponseModel;
import com.example.safaya.R;
import com.example.safaya.Utils.CommonUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AdminLogin extends Fragment {
    EditText password, username;
    private boolean passwordShown = false;
    Button login_btn;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login, container, false);
        password = view.findViewById(R.id.password);
        username = view.findViewById(R.id.username);
        login_btn = view.findViewById(R.id.loginButton);

        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if ( (!CommonUtil.IsNull(username.getText().toString().trim())) &&
                      (!CommonUtil.IsNull(password.getText().toString().trim()))
                   ) {
                       login(username.getText().toString(), password.getText().toString());
                 }
                else {
                    Toast.makeText(getActivity(), "Please enter username and password", Toast.LENGTH_SHORT).show();
                }

            }
        });

        return view;
    }

    private void login(String usrnm, String pswd) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<LoginResponseModel> call = apiInterface.Login(usrnm, pswd);
        call.enqueue(new Callback<LoginResponseModel>() {
            @Override
            public void onResponse(Call<LoginResponseModel> call, Response<LoginResponseModel> response) {
                LoginResponseModel loginResponseModel = response.body();
                if (loginResponseModel != null) {


                    List<LoginModel> list = loginResponseModel.getLoginModelres();
                    LoginModel model = list.get(0);
                    Log.d("sizeeeeee ...", model.getEmployee_Code());

                        String emp_code = model.getEmployee_Code();
                        Bundle bundle = new Bundle();
                        bundle.putString("Emp_Code", emp_code); // Put anything what you want

                        Fragment someFragment = new RegFrag();
                        someFragment.setArguments(bundle);
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.mainActivityFrameLayout, someFragment); // give your fragment container id in first parameter
                        transaction.commit();

                }
                else {
                    Toast.makeText(getActivity(), "Please enter valid username and password", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponseModel> call, Throwable t) {
                Log.d("fail...", t.getMessage());
            }
        });

    }


}
