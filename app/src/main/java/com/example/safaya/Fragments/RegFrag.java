package com.example.safaya.Fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.TouchDelegate;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.abdeveloper.library.MultiSelectDialog;
import com.abdeveloper.library.MultiSelectModel;
import com.example.safaya.Adapters.LanguageMultiSelectSpinnerAdapter;
import com.example.safaya.Adapters.MultiSelectSpinnerAdapter;
import com.example.safaya.Api.APIClient;
import com.example.safaya.Api.APIInterface;
import com.example.safaya.Api.ApiResponse;
import com.example.safaya.Listener;
import com.example.safaya.Model.CityModel;
import com.example.safaya.Model.DocumentModel;
import com.example.safaya.Model.EmployeeModel;
import com.example.safaya.Model.EmployeeResponseModel;
import com.example.safaya.Model.ExperienceModel;
import com.example.safaya.Model.InsertionModel;
import com.example.safaya.Model.LanguageModel;
import com.example.safaya.Model.LoginModel;
import com.example.safaya.Model.MultiSelectSpinnerModel;
import com.example.safaya.Model.RateModel;
import com.example.safaya.Model.ServiceModel;
import com.example.safaya.Model.ServiceRateModel;
import com.example.safaya.Model.WorkLocationModel;
import com.example.safaya.Model.WorkModel;
import com.example.safaya.Model.WorkTimingMoel;
import com.example.safaya.R;
import com.example.safaya.Utils.CommonUtil;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.tomtom.online.sdk.common.location.LatLngAcc;
import com.tomtom.online.sdk.search.OnlineSearchApi;
import com.tomtom.online.sdk.search.SearchApi;
import com.tomtom.online.sdk.search.SearchException;
import com.tomtom.online.sdk.search.api.SearchError;
import com.tomtom.online.sdk.search.api.fuzzy.FuzzySearchResultListener;
import com.tomtom.online.sdk.search.data.fuzzy.FuzzySearchQuery;
import com.tomtom.online.sdk.search.data.fuzzy.FuzzySearchQueryBuilder;
import com.tomtom.online.sdk.search.data.fuzzy.FuzzySearchResponse;
import com.tomtom.online.sdk.search.data.fuzzy.FuzzySearchResult;
import com.tomtom.online.sdk.search.extensions.SearchService;
import com.tomtom.online.sdk.search.extensions.SearchServiceConnectionCallback;
import com.tomtom.online.sdk.search.extensions.SearchServiceManager;
import com.tomtom.online.sdk.search.fuzzy.FuzzyOutcome;
import com.tomtom.online.sdk.search.fuzzy.FuzzyOutcomeCallback;
import com.tomtom.online.sdk.search.fuzzy.FuzzySearchDetails;
import com.tomtom.online.sdk.search.fuzzy.FuzzySearchSpecification;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Random;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.content.ContentValues.TAG;

public class RegFrag extends Fragment implements Listener {

    ArrayList<String> employeeNameArrayList, cityArrayList, genderArrayList, experienceArrayList, documentArrayList, serviceArrayList, childServiceArrayList, workTimeArrayList, languageArrayList, priceArrayList, childPriceArrayList, enrollmentCodeArrayList, searchLocationArrayList;
    private ArrayAdapter employeeCodeadapter, cityAdapter, genderAdapter, experienceAdapter, documentAdapter, serviceAdapter, childServiceAdapter, workTimeAdpter, languageAapter, priceAdpter, childPriceAdpter;
    ArrayAdapter<String> searchLocationAdapter;
    Spinner employeeCodeSpinner, citySpinner, genderSpinner, experienceSpinner, documentSpinner, serviceSpinner, workTimeSpinner, languageSpinner, priceSpinner;

    Spinner monthExperienceSpinner, yearsExperienceSpinner;
    ArrayList<String> monthExperienceArrayList, yearsExperienceArrayList;
    ArrayAdapter monthExperienceAdapter, yearsExperienceAdapter;
    int No_Of_Years, No_Of_Years_In_Month, Total_No_Of_Experience_Months, No_Of_Months;

    private ImageView selectPhotoGalleryImageView, selectPhotoCameraImageView, documentGalleryImageView, documentPhotoCameraImageView;


    TextView dateOfBirthTextView, selectPhotoTextView, enrollmentNumberTV,selectedDocumentTextView;

    DatePickerDialog.OnDateSetListener date;
    Calendar myCalendar;

    AutoCompleteTextView workLocationTextView;
    String search_text;
    SearchService tomtomSearch;

    Boolean isDataAvailable_city = false, isDataAvailable_workTime = false, isDataAvailable_language = false, isDataAvailable_experience = false, isDataAvailable_document = false, isDataAvailable_service = false, isDataAvailable_rate = false;

    private LinearLayout parentLinearLayout, addWorkLocationLayout, parentLinearLayoutServiceNPrice, addServiceLinearLayout;

    Spinner childServiceSpinner, childPriceSpinner;
    TextView childServiceTextView, childPriceTextView;
    LanguageMultiSelectSpinnerAdapter mylangAdapter;

    private static int AUTOCOMPLETE_REQUEST_CODE = 11;
    private static int PARENT_AUTOCOMPLETE_REQUEST_CODE = 21;
    private static int CAMERA_CODE = 1;
    private static int GALLERY_CODE = 2;
    boolean isTouch = false;

    //
    InsertionModel insertionObject;
    Button submitButton;
    int City_Id, Experience_Months, Document_Id;
    String Gender, Name, Contact_Number, Alt_Contact_Number, DOB, Resides_At;
    String Photo_File_Name, Document_File_Name;

    ArrayList<ServiceRateModel> ServiceRateList, childServiceRateList;
    ArrayList<WorkTimingMoel> WorkTimingList;
    ArrayList<WorkLocationModel> WorkLocationList;

    ServiceRateModel serviceRateModel;
    WorkLocationModel workLocationModel, childWorkLocationModel;
    WorkTimingMoel workTimingMoel;

    String city_name, gender_name, full_name, contact_number, alternate_contact_number, DOB_text, resides_at, work_location_text, work_timing;
    String language_name, experience, select_photo_name, document_name, service_name, rate_name;
    EditText fullNameEdittext, contactNumberEdittext, alternateContactNumberEdittext, residesAtEdittext;
    int RateId, ServiceId, DocumentId, ExperienceId, LanguageId, WorkTimeId;

    TextView languageMultiSelectTextView;
    MultiSelectDialog multiSelectDialog;
    ArrayList<MultiSelectModel> listOfLanguages;
    StringBuilder Language_Id_string;

    TextView workTimeMultiSelectTextView;
    MultiSelectDialog workTimeMultiSelectDialog;
    ArrayList<MultiSelectModel> listOfWorkTime;

    AutocompleteSupportFragment autocompleteFragment;

    String Emp_Code;
    private String SelectPhoto_GalleryImagePath = "", SelectPhoto__CameraImagePath = "";
    private String Document_GalleryImagePath = "", Document_CameraImagePath = "";
    String DocumentLayout = "DocumentLayout";
    String SelectPhotoLayout = "SelectPhotoLayout";

    private boolean SelectPhotoFromGalFlg = false, SelectPhotoFromCameraFlg = false;
    private boolean DocumentFromGalFlg = false, DocumentFromCameraFlg = false;

    TextView parentWorkLocationTextView;

    String selectedImagePath,CropedFile;

    boolean IsDocumentSelected = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_registration, container, false);
        try {

            assert getArguments() != null;
            Emp_Code = getArguments().getString("Emp_Code");
            Log.d("--- EMP CODE----", Emp_Code);

            serviceRateModel = new ServiceRateModel();
            workLocationModel = new WorkLocationModel();
            workTimingMoel = new WorkTimingMoel();

            ServiceRateList = new ArrayList<ServiceRateModel>();
            childServiceRateList = new ArrayList<ServiceRateModel>();
            WorkTimingList = new ArrayList<WorkTimingMoel>();
            WorkLocationList = new ArrayList<WorkLocationModel>();

            fullNameEdittext = view.findViewById(R.id.fullNameEdittext);
            contactNumberEdittext = view.findViewById(R.id.contactNumberEdittext);
            alternateContactNumberEdittext = view.findViewById(R.id.alternateContactNumberEdittext);
            residesAtEdittext = view.findViewById(R.id.residesAtEdittext);

            parentWorkLocationTextView = view.findViewById(R.id.parentWorkLocationTextView);
            parentWorkLocationTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG);
                    // Start the autocomplete intent.
                    Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields).setCountry("IN").build(getActivity().getApplicationContext());
                    startActivityForResult(intent, PARENT_AUTOCOMPLETE_REQUEST_CODE);
                }
            });


            submitButton = view.findViewById(R.id.submitButton);
            submitButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    insertData();
                }
            });

            selectPhotoTextView = view.findViewById(R.id.selectPhotoTextView);
            selectedDocumentTextView = view.findViewById(R.id.selectedDocumentTextView);

            parentLinearLayout = view.findViewById(R.id.parentLinearLayout);
            parentLinearLayoutServiceNPrice = view.findViewById(R.id.parentLinearLayoutServiceNPrice);

            addWorkLocationLayout = view.findViewById(R.id.addWorkLocationLayout);
            addWorkLocationLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onAddField(view);
                }
            });


            addServiceLinearLayout = view.findViewById(R.id.addServiceLinearLayout);
            addServiceLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onAddServiceNPriceField(view);
                }
            });

            searchLocationArrayList = new ArrayList<>();

       /*   TOMTOM API implementation

            workLocationTextView = view.findViewById(R.id.workLocationTextView);
            workLocationTextView.setThreshold(3);

            search_text = workLocationTextView.getText().toString();

            ServiceConnection serviceConnection = SearchServiceManager.createAndBind(getActivity().getApplicationContext(),
                    new SearchServiceConnectionCallback() {
                        @Override
                        public void onBindSearchService(SearchService searchService) {
                            // Log.d(TAG,"Search service retrieved");
                            tomtomSearch = searchService;
                        }
                    });

            workLocationTextView.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    LatLng mapCenter = (new LatLng(18.5204, 73.8567));
                    FuzzySearchQuery searchQuery = FuzzySearchQueryBuilder.create(workLocationTextView.getText().toString())
                            .withPosition(mapCenter)
                            .build();
                    tomtomSearch.search(searchQuery, new FuzzySearchResultListener() {
                        @Override
                        public void onSearchResult(FuzzySearchResponse fuzzySearchResponse) {
                            ImmutableList<FuzzySearchResult> results = fuzzySearchResponse.getResults();
                            //showSearchResults(results);
                            Log.d("--- SEARCH RES ----", String.valueOf(results.size()));

                            searchLocationArrayList.clear();
                            for (int i = 0; i < results.size(); i++) {
                                Log.d("--- LIST VALUE---", String.valueOf(results.get(i)));
                                searchLocationArrayList.add(results.get(i).getAddress().getFreeformAddress());
                            }

                            try {
                                Collections.reverse(searchLocationArrayList);
                                searchLocationAdapter = new ArrayAdapter<String>(Objects.requireNonNull(getActivity().getApplicationContext()), R.layout.custom_spinner_item, searchLocationArrayList);
                                //searchLocationAdapter.addAll(searchLocationArrayList);
                                workLocationTextView.setAdapter(searchLocationAdapter);
                                searchLocationAdapter.notifyDataSetChanged();
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }

                        @Override
                        public void onSearchError(SearchError searchError) {
                            Log.d("---- Search error----", searchError.getMessage());
                        }
                    });
                }

                @Override
                public void afterTextChanged(Editable editable) {
                }
            });  */

            //  GOOGLE PLACES API IMPLEMENTATION
            String apiKey = getString(R.string.google_places_api_key);

            /**
             * Initialize Places. For simplicity, the API key is hard-coded. In a production
             * environment we recommend using a secure mechanism to manage API keys.
             */
            if (!Places.isInitialized()) {
                Places.initialize(getActivity().getApplicationContext(), apiKey);
            }

// Create a new Places client instance.
            PlacesClient placesClient = Places.createClient(getContext().getApplicationContext());

            autocompleteFragment = (AutocompleteSupportFragment) getChildFragmentManager().findFragmentById(R.id.autocomplete_fragment);
            autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG));
            autocompleteFragment.setCountry("IN");
            autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
                @Override
                public void onPlaceSelected(Place place) {
                    // TODO: Get info about the selected place.
                    Log.d("Place: ", place.getName() + ", " + place.getId());
                    Toast.makeText(getActivity().getApplicationContext(), place.getName(), Toast.LENGTH_SHORT).show();

                    workLocationModel.setWork_Location_Name(place.getName());

                    LatLng latlan = place.getLatLng();
                    if (latlan != null) {
                        workLocationModel.setWork_Location_Lat(latlan.latitude);
                        workLocationModel.setWork_Location_Long(latlan.longitude);
                    }

                    WorkLocationList.add(workLocationModel);

                }

                @Override
                public void onError(Status status) {
                    Log.d("----An error occurred: ", String.valueOf(status));
                    Toast.makeText(getActivity().getApplicationContext(), status.toString(), Toast.LENGTH_SHORT).show();
                }
            });


          /*  AutocompleteSupportFragment autocomplete_fragment2 = (AutocompleteSupportFragment) getChildFragmentManager().findFragmentById(R.id.autocomplete_fragmentV2);
            autocomplete_fragment2.getView().setBackgroundColor(Color.WHITE);
            autocomplete_fragment2.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME));
            autocomplete_fragment2.setCountry("IN");
            autocomplete_fragment2.setOnPlaceSelectedListener(new PlaceSelectionListener() {
                @Override
                public void onPlaceSelected(Place place) {
                    // TODO: Get info about the selected place.
                    Log.d("Place: ", place.getName() + ", " + place.getId());
                    Toast.makeText(getActivity().getApplicationContext(), place.getName(), Toast.LENGTH_SHORT).show();
                }
                @Override
                public void onError(Status status) {
                    Log.d("----An error occurred: ", String.valueOf(status));
                    Toast.makeText(getActivity().getApplicationContext(), status.toString(), Toast.LENGTH_SHORT).show();
                }
            }); */

            myCalendar = Calendar.getInstance();
            dateOfBirthTextView = view.findViewById(R.id.dateOfBirthTextView);

            date = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                    myCalendar.set(Calendar.YEAR, year);
                    myCalendar.set(Calendar.MONTH, monthOfYear);
                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    updateLabel();
                }

            };
            dateOfBirthTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new DatePickerDialog(getContext(), date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            });

            selectPhotoGalleryImageView = view.findViewById(R.id.selectPhotoGalleryImageView);
            selectPhotoGalleryImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectImageFromGallery(SelectPhotoLayout);
                    SelectPhotoFromGalFlg = true;
                }
            });

            selectPhotoCameraImageView = view.findViewById(R.id.selectPhotoCameraImageView);
            selectPhotoCameraImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                     takeImageFromCamera(SelectPhotoLayout);
                      SelectPhotoFromCameraFlg = true;
                }
            });

            documentGalleryImageView = view.findViewById(R.id.documentGalleryImageView);
            documentGalleryImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectImageFromGallery(DocumentLayout);
                    DocumentFromGalFlg = true;
                }
            });
            documentPhotoCameraImageView = view.findViewById(R.id.documentPhotoCameraImageView);
            documentPhotoCameraImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                      takeImageFromCamera(DocumentLayout);
                       DocumentFromCameraFlg = true;
                }
            });

          /*  employeeNameArrayList = new ArrayList<String>();
            employeeNameArrayList.add(getString(R.string.employee_code));
            employeeCodeSpinner = view.findViewById(R.id.employeeCodeSpinner);
            getEmployee();*/

            cityArrayList = new ArrayList<String>();
            cityArrayList.add(getString(R.string.city));
            citySpinner = view.findViewById(R.id.citySpinner);
            getCity();

            enrollmentCodeArrayList = new ArrayList<String>();
            //int random = Random.nextInt(4);    // This returns a random int in the range [0, n-1].
            String codeList[] = getContext().getResources().getStringArray(R.array.EnrollmentNumberArray);
            enrollmentCodeArrayList.addAll(Arrays.asList(codeList));

            enrollmentNumberTV = view.findViewById(R.id.enrollmentNumberTV);

            citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String text = citySpinner.getSelectedItem().toString();
                    Random r = new Random();
                    int i = r.nextInt(5);

                    if (text.equalsIgnoreCase("Pune")) {
                        enrollmentNumberTV.setText(enrollmentCodeArrayList.get(0) + String.valueOf(i));

                    } else if (text.equalsIgnoreCase("Hydrabad")) {
                        enrollmentNumberTV.setText(enrollmentCodeArrayList.get(1) + String.valueOf(i));
                    } else if (text.equalsIgnoreCase("Chennai")) {
                        enrollmentNumberTV.setText(enrollmentCodeArrayList.get(2) + String.valueOf(i));
                    } else if (text.equalsIgnoreCase("Bangalore")) {
                        enrollmentNumberTV.setText(enrollmentCodeArrayList.get(3) + String.valueOf(i));
                    } else if (text.equalsIgnoreCase("Mumbai")) {
                        enrollmentNumberTV.setText(enrollmentCodeArrayList.get(4) + String.valueOf(i));
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    enrollmentNumberTV.setText(R.string.enrollment_no);
                }
            });

            genderSpinner = view.findViewById(R.id.genderSpinner);
            genderArrayList = new ArrayList<String>();
            String genList[] = getContext().getResources().getStringArray(R.array.GenderArray);
            genderArrayList.addAll(Arrays.asList(genList));


            genderAdapter = new ArrayAdapter(Objects.requireNonNull(getContext()), R.layout.custom_spinner_item, genderArrayList);
            genderAdapter.setDropDownViewResource(R.layout.custom_spinner_item);
            genderSpinner.setAdapter(genderAdapter);


         /*   experienceArrayList = new ArrayList<String>();
            experienceArrayList.add(getString(R.string.experience));
            experienceSpinner = view.findViewById(R.id.experienceSpinner);
           getExperience(); */

            monthExperienceSpinner = view.findViewById(R.id.monthExperienceSpinner);
            monthExperienceArrayList = new ArrayList<String>();
            String monthList[] = getContext().getResources().getStringArray(R.array.MonthArray);
            monthExperienceArrayList.addAll(Arrays.asList(monthList));

            monthExperienceAdapter = new ArrayAdapter(Objects.requireNonNull(getContext()), R.layout.custom_spinner_item, monthExperienceArrayList);
            monthExperienceAdapter.setDropDownViewResource(R.layout.custom_spinner_item);
            monthExperienceSpinner.setAdapter(monthExperienceAdapter);


            yearsExperienceSpinner = view.findViewById(R.id.yearsExperienceSpinner);
            yearsExperienceArrayList = new ArrayList<String>();
            String yearList[] = getContext().getResources().getStringArray(R.array.YearArray);
            yearsExperienceArrayList.addAll(Arrays.asList(yearList));

            yearsExperienceAdapter = new ArrayAdapter(Objects.requireNonNull(getContext()), R.layout.custom_spinner_item, yearsExperienceArrayList);
            yearsExperienceAdapter.setDropDownViewResource(R.layout.custom_spinner_item);
            yearsExperienceSpinner.setAdapter(yearsExperienceAdapter);


            documentArrayList = new ArrayList<String>();
            documentArrayList.add(getString(R.string.document));
            documentSpinner = view.findViewById(R.id.documentSpinner);
            getDocument();

            serviceArrayList = new ArrayList<String>();
            serviceArrayList.add(getString(R.string.service));
            serviceSpinner = view.findViewById(R.id.serviceSpinner);
            getService();

            childServiceArrayList = new ArrayList<String>();
            childServiceArrayList.add(getString(R.string.service));


            workTimeArrayList = new ArrayList<String>();
            workTimeArrayList.add(getString(R.string.work_time));
            workTimeSpinner = view.findViewById(R.id.workTimeSpinner);
            getWork();

            workTimeMultiSelectTextView = view.findViewById(R.id.workTimeMultiSelectTextView);
            workTimeMultiSelectTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    workTimeMultiSelectDialog.show(getChildFragmentManager(), "WorkTimeMultiSelectDialog");
                }
            });

            languageArrayList = new ArrayList<String>();
            languageArrayList.add(getString(R.string.language));
            languageSpinner = view.findViewById(R.id.languageSpinner);
            getLanguage();


            languageMultiSelectTextView = view.findViewById(R.id.languageMultiSelectTextView);
            languageMultiSelectTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    multiSelectDialog.show(getChildFragmentManager(), "multiSelectDialog");
                }
            });


            priceArrayList = new ArrayList<String>();
            priceArrayList.add("Rate");
            priceSpinner = view.findViewById(R.id.priceSpinner);
            getRate();


            //  priceArrayList.add(0, getString(R.string.price));
            //Log.d("---Price List size-----", String.valueOf(priceArrayList.size()));
            //   priceArrayList.add(getString(R.string.price));

        /*    priceAdpter = new ArrayAdapter(Objects.requireNonNull(getContext()), R.layout.custom_spinner_item, priceArrayList);
            priceAdpter.setDropDownViewResource(R.layout.custom_spinner_item);
            priceSpinner.setAdapter(priceAdpter); */


            childPriceArrayList = new ArrayList<String>();
            childPriceArrayList.add(getString(R.string.price));

            citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    City_Id = (int) citySpinner.getItemIdAtPosition(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {
                }
            });

            genderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Gender = genderSpinner.getItemAtPosition(position).toString();
                    if (Gender.trim().equalsIgnoreCase("Male")) {
                        Gender = "M";
                    } else {
                        Gender = "F";
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {
                }
            });

          /*  workTimeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    WorkTimeId = (int) workTimeSpinner.getItemIdAtPosition(position);
                  //  WorkTimingList.add(WorkTimeId);
                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {
                }
            }); */

            languageSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    LanguageId = (int) languageSpinner.getItemIdAtPosition(position);
                    //   Language_Id_string = languageSpinner.getItemAtPosition(position).toString();
                    //  Language_Id_string += LanguageId;

                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {
                }
            });

         /*   experienceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    ExperienceId =  (int) experienceSpinner.getItemIdAtPosition(position);
                }
                @Override
                public void onNothingSelected(AdapterView<?> arg0) {
                }
            }); */

            yearsExperienceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    No_Of_Years = (int) yearsExperienceSpinner.getItemIdAtPosition(position);
                    Log.d("POSITION- YR----------", String.valueOf(position));
                    if (position == 0) {
                        Total_No_Of_Experience_Months = 0;
                    }

                    if (No_Of_Years > 0) {
                        No_Of_Years_In_Month = (No_Of_Years) * 12;
                        Total_No_Of_Experience_Months = No_Of_Years_In_Month;
                        Log.d("------- EXP YEAR--", String.valueOf(Total_No_Of_Experience_Months));
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {
                }
            });

            monthExperienceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Log.d("POSITION- MNTH-------", String.valueOf(position));
                    No_Of_Months = (int) monthExperienceSpinner.getItemIdAtPosition(position);
                    if (position == 0 || position == 1) {
                        No_Of_Months = 0;
                    }
                    Total_No_Of_Experience_Months = No_Of_Years_In_Month + (No_Of_Months - 1);
                    Log.d("------- Total EXP --", String.valueOf(Total_No_Of_Experience_Months));
                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {
                }
            });

            documentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    DocumentId = (int) documentSpinner.getItemIdAtPosition(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {
                }
            });

            serviceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    ServiceId = (int) serviceSpinner.getItemIdAtPosition(position);
                    serviceRateModel.setService_Id(ServiceId);
                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {
                }
            });

            priceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    RateId = (int) priceSpinner.getItemIdAtPosition(position);
                    serviceRateModel.setRate(RateId);
                    ServiceRateList.add(serviceRateModel);
                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {
                }
            });

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return view;
    }

    public void insertData() {
        try {
            //  validate();
            if (citySpinner.getSelectedItemId() <= 0) {
                Toast.makeText(getActivity().getApplicationContext(), "Please select city", Toast.LENGTH_SHORT).show();
            }
            else if (genderSpinner.getSelectedItemId() <= 0) {
                Toast.makeText(getActivity().getApplicationContext(), "Please select gender", Toast.LENGTH_SHORT).show();
            }
            else if (CommonUtil.isEmpty(fullNameEdittext.getText().toString().trim())) {
                fullNameEdittext.setError("Enter the name");
                fullNameEdittext.requestFocus();
            }
            else if ((CommonUtil.isEmpty(contactNumberEdittext.getText().toString().trim())) || (contactNumberEdittext.getText().toString().trim()).length() < 10) {
                contactNumberEdittext.setError("Enter 10 digit contact number ");
                contactNumberEdittext.requestFocus();
            }
            else if ((CommonUtil.isEmpty(alternateContactNumberEdittext.getText().toString())) || (alternateContactNumberEdittext.getText().toString().trim()).length() < 10) {
                alternateContactNumberEdittext.setError("Enter 10 digit alternate contact number");
                alternateContactNumberEdittext.requestFocus();
            }
            else if (CommonUtil.isEmpty(dateOfBirthTextView.getText().toString().trim())) {
                dateOfBirthTextView.setError("Select the date");
                dateOfBirthTextView.requestFocus();
            }
            else if (CommonUtil.isEmpty(residesAtEdittext.getText().toString().trim())) {
                residesAtEdittext.setError("Enter the address");
                residesAtEdittext.requestFocus();
            }
            else if (CommonUtil.isEmpty(parentWorkLocationTextView.getText().toString().trim())) {
                parentWorkLocationTextView.setError("Select the location");
                parentWorkLocationTextView.requestFocus();
            }
            else if (CommonUtil.isEmpty(workTimeMultiSelectTextView.getText().toString().trim())) {
                workTimeMultiSelectTextView.setError("Select the Worktime");
                workTimeMultiSelectTextView.requestFocus();
            }
            else if (CommonUtil.isEmpty(languageMultiSelectTextView.getText().toString().trim())) {
                languageMultiSelectTextView.setError("Select the language");
                languageMultiSelectTextView.requestFocus();
            }

            else if (yearsExperienceSpinner.getSelectedItemId() <= 0) {
                Toast.makeText(getActivity().getApplicationContext(), "Please select year", Toast.LENGTH_SHORT).show();
            }
            else if (monthExperienceSpinner.getSelectedItemId() <= 0) {
                Toast.makeText(getActivity().getApplicationContext(), "Please select month", Toast.LENGTH_SHORT).show();
            }
            else if (CommonUtil.isEmpty(selectPhotoTextView.getText().toString().trim())) {
                selectPhotoTextView.setError("Select photo / Take photo");
                selectPhotoTextView.requestFocus();
            }

            else if (documentSpinner.getSelectedItemId() <= 0) {
                Toast.makeText(getActivity().getApplicationContext(), "Please select document", Toast.LENGTH_SHORT).show();
            }
           else if (CommonUtil.isEmpty(selectedDocumentTextView.getText().toString().trim())) {
                selectedDocumentTextView.setVisibility(View.VISIBLE);
                selectedDocumentTextView.setError("Select document image");
                selectedDocumentTextView.requestFocus();
            }
            else if (serviceSpinner.getSelectedItemId() <= 0) {
                Toast.makeText(getActivity().getApplicationContext(), "Please select service", Toast.LENGTH_SHORT).show();
            }
            else if (priceSpinner.getSelectedItemId() <= 0) {
                Toast.makeText(getActivity().getApplicationContext(), "Please select rate", Toast.LENGTH_SHORT).show();
            }
            else {
                insertionObject = new InsertionModel();

                insertionObject.setEmp_Code(Emp_Code);
                insertionObject.setEnrollment_Number("sample string 3");
                insertionObject.setCity_Id(City_Id);
                insertionObject.setGender(Gender);
                insertionObject.setName(fullNameEdittext.getText().toString());
                insertionObject.setContact_Number(contactNumberEdittext.getText().toString());
                insertionObject.setAlt_Contact_Number(alternateContactNumberEdittext.getText().toString());
                insertionObject.setDOB(DOB_text);
                insertionObject.setResides_At(residesAtEdittext.getText().toString());
                insertionObject.setLanguage_Id(Language_Id_string);
                insertionObject.setExperience_Months(Total_No_Of_Experience_Months);

                if (!CommonUtil.IsNull(SelectPhoto_GalleryImagePath)) {
                    insertionObject.setPhoto_File_Name(CommonUtil.ImageToBase64(SelectPhoto_GalleryImagePath));
                } else {
                    insertionObject.setPhoto_File_Name(CommonUtil.ImageToBase64(SelectPhoto__CameraImagePath));
                   // insertionObject.setPhoto_File_Name("");
                }

                if (!CommonUtil.IsNull(Document_GalleryImagePath)) {
                    insertionObject.setDocument_File_Name(CommonUtil.ImageToBase64(Document_GalleryImagePath));
                } else {
                     insertionObject.setDocument_File_Name(CommonUtil.ImageToBase64(Document_CameraImagePath));
                  //  insertionObject.setDocument_File_Name("");
                }


                insertionObject.setDocument_Id(DocumentId);


                ServiceRateList.clear();

                ServiceRateModel serviceRateModelP = new ServiceRateModel();
                serviceRateModelP.setService_Id((int) serviceSpinner.getSelectedItemId());
                serviceRateModelP.setRate((int) priceSpinner.getSelectedItemId());

                ServiceRateList.add(serviceRateModelP);


                int count = parentLinearLayoutServiceNPrice.getChildCount();
                Log.d("---- D COUNT---", String.valueOf(count));
                View view;

                LinearLayout parentserviceLinearLayout, serviceLinearLayout;

                for (int i = 0; i < count; i++) {
                    view = parentLinearLayoutServiceNPrice.getChildAt(i);

                    serviceLinearLayout = view.findViewById(R.id.serviceLinearLayout);
                    parentserviceLinearLayout = view.findViewById(R.id.parentserviceLinearLayout);

                    Spinner dynamicServiceSpinner = view.findViewById(R.id.serviceSpinnerV2);
                    Spinner dynamicPriceSpinner = view.findViewById(R.id.priceSpinnerV2);

                    ServiceRateModel serviceRateModel2 = new ServiceRateModel();
                    serviceRateModel2.setService_Id((int) dynamicServiceSpinner.getSelectedItemId());
                    serviceRateModel2.setRate((int) dynamicPriceSpinner.getSelectedItemId());

             /*   Log.d("--Dynamic Service Id--", String.valueOf(dynamicServiceSpinner.getSelectedItemId()));
                Log.d("-- Dynamic Price Id--", String.valueOf(dynamicPriceSpinner.getSelectedItemId()));*/

                    ServiceRateList.add(serviceRateModel2);

                }
                Log.d("----servicerate list --", String.valueOf(ServiceRateList.size()));

                insertionObject.setServiceRateList(ServiceRateList);
                insertionObject.setWorkLocationList(WorkLocationList);
                insertionObject.setWorkTimingList(WorkTimingList);


                APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
                Call<InsertionModel> call = apiInterface.insertUserData(insertionObject);
                call.enqueue(new Callback<InsertionModel>() {
                    @Override
                    public void onResponse(Call<InsertionModel> call, Response<InsertionModel> response) {
                        if (response.body() != null) {
                            if (response.isSuccessful()) {
                                Log.d("------INSERTION SUCCESS", response.body().toString());
                                Toast.makeText(getActivity(), "Record Inserted Successfully", Toast.LENGTH_SHORT).show();
                                resetFormData();
                            }

                            // Log.d("----Response Body-----", String.valueOf(response.body().isSuccess()));
                        }
                    }

                    @Override
                    public void onFailure(Call<InsertionModel> call, Throwable t) {
                        Log.d("Insertion onFailure----", t.getMessage());
                    }
                });

            }


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void resetFormData() {

        getFragmentManager().beginTransaction().detach(this).attach(this).commit();

        citySpinner.setSelection(0);
        genderSpinner.setSelection(0);

        fullNameEdittext.setText("");
        contactNumberEdittext.setText("");
        alternateContactNumberEdittext.setText("");

        dateOfBirthTextView.setHint(R.string.date_of_birth);

        residesAtEdittext.setText("");

        parentWorkLocationTextView.setHint(R.string.work_location);
        workTimeMultiSelectTextView.setHint(R.string.work_time);
        languageMultiSelectTextView.setHint(R.string.language);

        yearsExperienceSpinner.setSelection(0);
        monthExperienceSpinner.setSelection(0);

        selectPhotoTextView.setHint(R.string.select_photo_take_photo);

        selectedDocumentTextView.setHint("");
        selectedDocumentTextView.setVisibility(View.GONE);

        documentSpinner.setSelection(0);
        serviceSpinner.setSelection(0);
        priceSpinner.setSelection(0);
        autocompleteFragment.setText("");

    }

    public void validate() {
        try {

       /*     if (citySpinner != null && citySpinner.getSelectedItem() != null) {
                city_name = (String) citySpinner.getSelectedItem();
            } else {
                citySpinner.setFocusable(true);

            }

            if (genderSpinner != null && genderSpinner.getSelectedItem() != null) {
                gender_name = (String) genderSpinner.getSelectedItem();
            } else {
                genderSpinner.setFocusable(true);
            }  */


/*
            if (!CommonUtil.isEmpty(fullNameEdittext.getText().toString())) {
                full_name = fullNameEdittext.getText().toString();
            } else {
                fullNameEdittext.requestFocus();
            }

            if (!CommonUtil.isEmpty(contactNumberEdittext.getText().toString())) {
                contact_number = contactNumberEdittext.getText().toString();
            } else {
                contactNumberEdittext.requestFocus();
            }

            if (!CommonUtil.isEmpty(alternateContactNumberEdittext.getText().toString())) {
                alternate_contact_number = alternateContactNumberEdittext.getText().toString();
            } else {
                alternateContactNumberEdittext.requestFocus();
            }

            if (!CommonUtil.isEmpty(residesAtEdittext.getText().toString())) {
                resides_at = residesAtEdittext.getText().toString();
            } else {
                residesAtEdittext.requestFocus();
            }
*/

            if (CommonUtil.isEmpty(fullNameEdittext.getText().toString().trim())) {
                fullNameEdittext.setError("Enter the name");
                fullNameEdittext.requestFocus();
            } else if ((CommonUtil.isEmpty(contactNumberEdittext.getText().toString().trim())) || (contactNumberEdittext.getText().toString().trim()).length() < 10) {
                contactNumberEdittext.setError("Enter 10 digit contact number ");
                contactNumberEdittext.requestFocus();
            } else if ((CommonUtil.isEmpty(alternateContactNumberEdittext.getText().toString())) || (alternateContactNumberEdittext.getText().toString().trim()).length() < 10) {
                alternateContactNumberEdittext.setError("Enter 10 digit alternate contact number");
                alternateContactNumberEdittext.requestFocus();
            } else if (CommonUtil.isEmpty(residesAtEdittext.getText().toString().trim())) {
                residesAtEdittext.setError("Enter the address");
                residesAtEdittext.requestFocus();
            }

        /*    if (workTimeSpinner != null && workTimeSpinner.getSelectedItem() != null) {
                work_timing = (String) workTimeSpinner.getSelectedItem();
            } else {
                workTimeSpinner.requestFocus();
            }

            if (languageSpinner != null && languageSpinner.getSelectedItem() != null) {
                language_name = (String) languageSpinner.getSelectedItem();
            } else {
                languageSpinner.requestFocus();
            }  */

        /*    if(experienceSpinner != null && experienceSpinner.getSelectedItem() !=null ) {
                experience  = (String)experienceSpinner.getSelectedItem();
            }else {
                experienceSpinner.setFocusable(true);
            } */


       /* if (monthExperienceSpinner == null && monthExperienceSpinner.getSelectedItem() == null) {
                monthExperienceSpinner.setFocusable(true);
            }

            if (documentSpinner != null && documentSpinner.getSelectedItem() != null) {
                document_name = (String) documentSpinner.getSelectedItem();
            } else {
                documentSpinner.setFocusable(true);
            }

            if (serviceSpinner != null && serviceSpinner.getSelectedItem() != null) {
                service_name = (String) serviceSpinner.getSelectedItem();
            } else {
                serviceSpinner.setFocusable(true);
            }

            if (priceSpinner != null && priceSpinner.getSelectedItem() != null) {
                rate_name = (String) priceSpinner.getSelectedItem();
            } else {
                priceSpinner.setFocusable(true);
            }
          */

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void onAddField(View v) {
      /*  LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.add_button_layout, null);
        parentLinearLayout.setVisibility(View.VISIBLE);
        parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1); */
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG);

        // Start the autocomplete intent.
        Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields).setCountry("IN").build(getActivity().getApplicationContext());
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);

       /*
       AutocompleteSupportFragment searchLocationACTV = rowView.findViewById(R.id.autocomplete_fragment);
      searchLocationACTV.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                LatLng mapCenter = (new LatLng(18.5204, 73.8567));
                FuzzySearchQuery searchQuery = FuzzySearchQueryBuilder.create(searchLocationACTV.getText().toString())
                        .withPosition(mapCenter)
                        .build();
                tomtomSearch.search(searchQuery, new FuzzySearchResultListener() {
                    @Override
                    public void onSearchResult(FuzzySearchResponse fuzzySearchResponse) {
                        ImmutableList<FuzzySearchResult> results = fuzzySearchResponse.getResults();
                        Log.d("--- SEARCH RES ----", String.valueOf(results.size()));

                        searchLocationArrayList.clear();
                        for (int i = 0; i < results.size(); i++) {
                            Log.d("--- LIST VALUE---", String.valueOf(results.get(i)));
                            searchLocationArrayList.add(results.get(i).getAddress().getFreeformAddress());
                        }

                        try {
                            Collections.reverse(searchLocationArrayList);
                            searchLocationAdapter = new ArrayAdapter<String>(Objects.requireNonNull(getActivity().getApplicationContext()), R.layout.custom_spinner_item, searchLocationArrayList);
                            searchLocationACTV.setAdapter(searchLocationAdapter);
                            searchLocationAdapter.notifyDataSetChanged();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                    @Override
                    public void onSearchError(SearchError searchError) {
                        Log.d("---- Search error----", searchError.getMessage());
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });  */

       /*
        //  GOOGLE PLACES API IMPLEMENTATION
        String apiKey = getString(R.string.google_places_api_key);
        if (!Places.isInitialized()) {
            Places.initialize(getActivity().getApplicationContext(), apiKey);
        }
        // Set the fields to specify which types of place data to
        // return after the user has made a selection.

        // Create a new Places client instance.
        PlacesClient placesClient = Places.createClient(getContext().getApplicationContext());
        AutocompleteSupportFragment autocompleteFragment3 = (AutocompleteSupportFragment) getChildFragmentManager().findFragmentById(R.id.autocomplete_fragment3);
        autocompleteFragment3.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME));
        autocompleteFragment3.setCountry("IN");
        autocompleteFragment3.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                Log.d("Place: ", place.getName() + ", " + place.getId());
                Toast.makeText(getActivity().getApplicationContext(), place.getName(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(Status status) {
                Log.d("----An error occurred: ", String.valueOf(status));
                Toast.makeText(getActivity().getApplicationContext(), status.toString(), Toast.LENGTH_SHORT).show();
            }
        });

        View view = autocompleteFragment3.getView();

        LinearLayout linearlayout = (LinearLayout) rowView.findViewById(R.id.cardView);
        linearlayout.setOrientation(LinearLayout.VERTICAL);

        LinearLayout.LayoutParams buttonParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        buttonParams.setMargins(0, 0, 0, 10);

        linearlayout.addView(view,buttonParams);  */

    /*    final AutocompleteSupportFragment myButton = new AutocompleteSupportFragment();
        myButton.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME));
        myButton.setCountry("IN");
        myButton.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                Log.d("Place: ", place.getName() + ", " + place.getId());
                Toast.makeText(getActivity().getApplicationContext(), place.getName(), Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onError(Status status) {
                Log.d("----An error occurred: ", String.valueOf(status));
                Toast.makeText(getActivity().getApplicationContext(), status.toString(), Toast.LENGTH_SHORT).show();
            }
        });


        LinearLayout linearlayout = (LinearLayout) rowView.findViewById(R.id.cardView);
        linearlayout.setOrientation(LinearLayout.VERTICAL);

        LinearLayout.LayoutParams buttonParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        buttonParams.setMargins(0, 0, 0, 10);

        linearlayout.addView(myButton,buttonParams);  */

     /*   ImageView cancelWorkLocationImgBtn = rowView.findViewById(R.id.cancelWorkLocationImgBtn);
        cancelWorkLocationImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((LinearLayout) rowView.getParent()).removeView(rowView);
                Log.d("--- Remove btn----", String.valueOf(v.getParent()));
            }
        });  */
    }

    public void onDelete(View v) {
//        LayoutInflater inflater=(LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        final View rowView=inflater.inflate(R.layout.add_button_layout, null);
//
//        ImageView cancelLocationIV = rowView.findViewById(R.id.cancelWorkLocationImgBtn);
//        cancelLocationIV.setOnClickListener(this::onDelete);

        parentLinearLayout.removeView((View) v.getParent());
    }

    public void onAddServiceNPriceField(View v) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.add_service_price_layout, null);

        parentLinearLayoutServiceNPrice.setVisibility(View.VISIBLE);
        parentLinearLayoutServiceNPrice.addView(rowView, parentLinearLayoutServiceNPrice.getChildCount() - 1);

    /*    childServiceTextView = rowView.findViewById(R.id.serviceTextView);
        childPriceTextView = rowView.findViewById(R.id.priceTextView);

        childServiceTextView.setId(View.generateViewId());
        childPriceTextView.setId(View.generateViewId());


        getService();
        getRate();

        ServiceRateModel serviceRateModel2 = new ServiceRateModel();

        serviceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int service_Id = (int) serviceSpinner.getItemIdAtPosition(position);
                serviceRateModel2.setService_Id(service_Id);
                String service_name = serviceSpinner.getSelectedItem().toString();
                childServiceTextView.setText(service_name);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        priceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int rate_Id = (int) priceSpinner.getSelectedItemId();
                serviceRateModel2.setRate(rate_Id);

                String price_name = priceSpinner.getSelectedItem().toString();
                childPriceTextView.setText(price_name);

               ServiceRateList.add(serviceRateModel2);

                Log.d("----- RATE LIST--", String.valueOf(ServiceRateList.size()));
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
      */

        childServiceSpinner = rowView.findViewById(R.id.serviceSpinnerV2);
        childPriceSpinner = rowView.findViewById(R.id.priceSpinnerV2);

       /* childServiceSpinner.setId(View.generateViewId());
        childPriceSpinner.setId(View.generateViewId());*/

        getChildService();
        getChileRate();

    /*    ServiceRateModel serviceRateModel2 = new ServiceRateModel();

      childPriceSpinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                   isTouch = true;
                   return false;
            }
        });

      childServiceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ServiceId = (int) childServiceSpinner.getItemIdAtPosition(position);
                serviceRateModel2.setService_Id(ServiceId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

      childPriceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                RateId = (int) childPriceSpinner.getSelectedItemId();
                serviceRateModel2.setRate(RateId);
                if(isTouch){
                   // ServiceRateList.remove(1);
                    ServiceRateList.add(serviceRateModel2);
                    isTouch = false;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        }); */


        ImageView cancelWorkLocationImgBtn = rowView.findViewById(R.id.cancelServiceImgBtn);
        cancelWorkLocationImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((LinearLayout) rowView.getParent()).removeView(rowView);

            /*    int childServiceid = childServiceTextView.getId();
                int childPriceid = childPriceTextView.getId();
                Log.d("---- REMOVED child serv", String.valueOf(childServiceid));
                Log.d("----REMOVED child price", String.valueOf(childPriceid));

                ServiceRateModel serviceRateModel2 = new ServiceRateModel();
             /*   int service_id = (int) childServiceTextView.getItemIdAtPosition(childServiceid);
                int price_id = (int) childPriceTextView.getItemIdAtPosition(childPriceid); */
//
//                serviceRateModel2.setRate(price_id);
//                serviceRateModel2.setService_Id(service_id);


         /*       for (int i = 0; i < ServiceRateList.size(); i++) {
                    if ((ServiceRateList.get(i).getService_Id() == (serviceRateModel2.getService_Id())) &&
                            (ServiceRateList.get(i).getRate() == (serviceRateModel2.getRate()))
                    ) {
                        ServiceRateList.remove(i);
                    }
                }
                Log.d("----servicerate list --", String.valueOf(ServiceRateList.size()));  */

            }
        });
    }

    private void getChildService() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<List<ServiceModel>> call = apiInterface.getService();
        call.enqueue(new Callback<List<ServiceModel>>() {
            @Override
            public void onResponse(Call<List<ServiceModel>> call, Response<List<ServiceModel>> response) {
                if (response.body() != null) {
                    List<ServiceModel> serviceResponseList = response.body();
                    childServiceArrayList.clear();
                    childServiceArrayList.add(getString(R.string.service));
                    if (serviceResponseList.size() > 0) {
                        isDataAvailable_service = true;
                        for (int i = 0; i < serviceResponseList.size(); i++) {
                            String serviceName = serviceResponseList.get(i).getService_Name();
                            childServiceArrayList.add(serviceName);
                        }
                        childServiceAdapter = new ArrayAdapter(Objects.requireNonNull(getContext()), R.layout.custom_spinner_item, childServiceArrayList);
                        childServiceAdapter.setDropDownViewResource(R.layout.custom_spinner_item);
                        childServiceSpinner.setAdapter(childServiceAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<ServiceModel>> call, Throwable t) {
                Log.d("------- onFailure----", t.getMessage());

            }
        });
    }

    private void getChileRate() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<List<RateModel>> call = apiInterface.getRate();
        call.enqueue(new Callback<List<RateModel>>() {
            @Override
            public void onResponse(Call<List<RateModel>> call, Response<List<RateModel>> response) {
                if (response.body() != null) {
                    List<RateModel> rateResponseList = response.body();

                    if (rateResponseList.size() > 0) {
                        isDataAvailable_rate = true;
                        childPriceArrayList.clear();
                        childPriceArrayList.add(getString(R.string.price));
                        for (int i = 0; i < rateResponseList.size(); i++) {
                            String rate = rateResponseList.get(i).getRate();
                            if (rate != null)
                                childPriceArrayList.add(rate);
                        }

                        childPriceAdpter = new ArrayAdapter(Objects.requireNonNull(getContext()), R.layout.custom_spinner_item, childPriceArrayList);
                        childPriceAdpter.setDropDownViewResource(R.layout.custom_spinner_item);
                        childPriceSpinner.setAdapter(childPriceAdpter);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<RateModel>> call, Throwable t) {
                Log.d("------- onFailure----", t.getMessage());

            }
        });
    }

    private void getEmployee() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<List<EmployeeModel>> call = apiInterface.getEmployee();
        call.enqueue(new Callback<List<EmployeeModel>>() {
            @Override
            public void onResponse(Call<List<EmployeeModel>> call, Response<List<EmployeeModel>> response) {
                if (response.body() != null) {
                    List<EmployeeModel> employeeResponseList = response.body();
                    Log.d("------- size---------", String.valueOf(employeeResponseList.size()));
                    employeeNameArrayList.clear();
                    employeeNameArrayList.add(getString(R.string.employee_code));
                    if (employeeResponseList.size() > 0) {
                        for (int i = 0; i < employeeResponseList.size(); i++) {
                            String employeeName = employeeResponseList.get(i).getEmployee_Name();
                            employeeNameArrayList.add(employeeName);
                        }
//                        employeeCodeadapter = new ArrayAdapter(Objects.requireNonNull(getContext()), android.R.layout.simple_spinner_item, employeeNameArrayList);
//                        employeeCodeadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                        employeeCodeadapter = new ArrayAdapter(Objects.requireNonNull(getContext()), R.layout.custom_spinner_item, employeeNameArrayList);
                        employeeCodeadapter.setDropDownViewResource(R.layout.custom_spinner_item);

                        employeeCodeSpinner.setAdapter(employeeCodeadapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<EmployeeModel>> call, Throwable t) {
                Log.d("------- onFailure----", t.getMessage());

            }
        });
    }

    private void getCity() {
        ProgressBar progressBarV2 = new ProgressBar(getActivity().getApplicationContext(), null, android.R.attr.progressBarStyleSmall);
        progressBarV2.setVisibility(View.VISIBLE);
        progressBarV2.setIndeterminate(true);

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<List<CityModel>> call = apiInterface.getCity();
        call.enqueue(new Callback<List<CityModel>>() {
            @Override
            public void onResponse(Call<List<CityModel>> call, Response<List<CityModel>> response) {
                if (response.body() != null) {
                    List<CityModel> cityResponseList = response.body();
                    Log.d("------- city size------", String.valueOf(cityResponseList.size()));
                    progressBarV2.setVisibility(View.GONE);
                    cityArrayList.clear();
                    cityArrayList.add(getString(R.string.city));
                    if (cityResponseList.size() > 0) {
                        isDataAvailable_city = true;
                        for (int i = 0; i < cityResponseList.size(); i++) {
                            String cityName = cityResponseList.get(i).getCity_Name();
                            cityArrayList.add(cityName);
                        }
//                        cityAdapter = new ArrayAdapter(Objects.requireNonNull(getContext()), android.R.layout.simple_spinner_item, cityArrayList);
//                        cityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        cityAdapter = new ArrayAdapter(Objects.requireNonNull(getContext()), R.layout.custom_spinner_item, cityArrayList);
                        cityAdapter.setDropDownViewResource(R.layout.custom_spinner_item);
                        citySpinner.setAdapter(cityAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<CityModel>> call, Throwable t) {
                progressBarV2.setVisibility(View.VISIBLE);
                Log.d("------- onFailure----", t.getMessage());

            }
        });
    }

    private void getExperience() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<List<ExperienceModel>> call = apiInterface.getExperience();
        call.enqueue(new Callback<List<ExperienceModel>>() {
            @Override
            public void onResponse(Call<List<ExperienceModel>> call, Response<List<ExperienceModel>> response) {
                if (response.body() != null) {
                    List<ExperienceModel> experienceResponseList = response.body();
                    Log.d("------- size---------", String.valueOf(experienceResponseList.size()));

                    experienceArrayList.clear();
                    experienceArrayList.add(getString(R.string.experience));
                    if (experienceResponseList.size() > 0) {
                        isDataAvailable_experience = true;
                        for (int i = 0; i < experienceResponseList.size(); i++) {
                            String experience_name = experienceResponseList.get(i).getExperience_Name();
                            experienceArrayList.add(experience_name);
                        }
//                        experienceAdapter = new ArrayAdapter(Objects.requireNonNull(getContext()), android.R.layout.simple_spinner_item, experienceArrayList);
//                        experienceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        experienceAdapter = new ArrayAdapter(Objects.requireNonNull(getContext()), R.layout.custom_spinner_item, experienceArrayList);
                        experienceAdapter.setDropDownViewResource(R.layout.custom_spinner_item);
                        experienceSpinner.setAdapter(experienceAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<ExperienceModel>> call, Throwable t) {
                Log.d("------- onFailure----", t.getMessage());

            }
        });
    }

    private void getDocument() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<List<DocumentModel>> call = apiInterface.getDocument();
        call.enqueue(new Callback<List<DocumentModel>>() {
            @Override
            public void onResponse(Call<List<DocumentModel>> call, Response<List<DocumentModel>> response) {
                if (response.body() != null) {
                    List<DocumentModel> documentResponseList = response.body();
                    Log.d("------- size---------", String.valueOf(documentResponseList.size()));
                    documentArrayList.clear();
                    documentArrayList.add(getString(R.string.document));
                    if (documentResponseList.size() > 0) {
                        isDataAvailable_document = true;
                        for (int i = 0; i < documentResponseList.size(); i++) {
                            String documentName = documentResponseList.get(i).getDocument_Name();
                            documentArrayList.add(documentName);
                        }
//                        documentAdapter = new ArrayAdapter(Objects.requireNonNull(getContext()), android.R.layout.simple_spinner_item, documentArrayList);
//                        documentAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        documentAdapter = new ArrayAdapter(Objects.requireNonNull(getContext()), R.layout.custom_spinner_item, documentArrayList);
                        documentAdapter.setDropDownViewResource(R.layout.custom_spinner_item);

                        documentSpinner.setAdapter(documentAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<DocumentModel>> call, Throwable t) {
                Log.d("------- onFailure----", t.getMessage());

            }
        });
    }

    private void getService() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<List<ServiceModel>> call = apiInterface.getService();
        call.enqueue(new Callback<List<ServiceModel>>() {
            @Override
            public void onResponse(Call<List<ServiceModel>> call, Response<List<ServiceModel>> response) {
                if (response.body() != null) {
                    List<ServiceModel> serviceResponseList = response.body();
                    Log.d("------- size---------", String.valueOf(serviceResponseList.size()));
                    serviceArrayList.clear();
                    serviceArrayList.add(getString(R.string.service));
                    if (serviceResponseList.size() > 0) {
                        isDataAvailable_service = true;
                        for (int i = 0; i < serviceResponseList.size(); i++) {
                            String serviceName = serviceResponseList.get(i).getService_Name();
                            serviceArrayList.add(serviceName);
                        }
//                        serviceAdapter = new ArrayAdapter(Objects.requireNonNull(getContext()), android.R.layout.simple_spinner_item, serviceArrayList);
//                        serviceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        serviceAdapter = new ArrayAdapter(Objects.requireNonNull(getContext()), R.layout.custom_spinner_item, serviceArrayList);
                        serviceAdapter.setDropDownViewResource(R.layout.custom_spinner_item);
                        serviceSpinner.setAdapter(serviceAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<ServiceModel>> call, Throwable t) {
                Log.d("------- onFailure----", t.getMessage());

            }
        });
    }

    private void getRate() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<List<RateModel>> call = apiInterface.getRate();
        call.enqueue(new Callback<List<RateModel>>() {
            @Override
            public void onResponse(Call<List<RateModel>> call, Response<List<RateModel>> response) {
                if (response.body() != null) {
                    List<RateModel> rateResponseList = response.body();
                    Log.d("------- rate ---------", String.valueOf(rateResponseList.size()));

                    priceArrayList.clear();
                    if (rateResponseList.size() > 0) {
                        isDataAvailable_rate = true;
                        priceArrayList.add(getString(R.string.price));
                        for (int i = 0; i < rateResponseList.size(); i++) {
                            String rate = rateResponseList.get(i).getRate();
                            if (rate != null)
                                priceArrayList.add(rate);
                        }
                        priceAdpter = new ArrayAdapter(Objects.requireNonNull(getContext()), R.layout.custom_spinner_item, priceArrayList);
                        priceAdpter.setDropDownViewResource(R.layout.custom_spinner_item);
                        priceSpinner.setAdapter(priceAdpter);
//                        priceAdpter = new ArrayAdapter(Objects.requireNonNull(getContext()), android.R.layout.simple_spinner_item, priceArrayList);
//                        priceAdpter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                     /*   priceAdpter = new ArrayAdapter(Objects.requireNonNull(getContext()), R.layout.custom_spinner_item, priceArrayList);
                        priceAdpter.setDropDownViewResource(R.layout.custom_spinner_item);*/

                     /*   priceAdpter.addAll(priceArrayList);
                        priceSpinner.setAdapter(priceAdpter);
                        priceAdpter.notifyDataSetChanged();*/
                    }
                }
            }

            @Override
            public void onFailure(Call<List<RateModel>> call, Throwable t) {
                Log.d("------- onFailure----", t.getMessage());
            }
        });

    }

    private void getWork() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<List<WorkModel>> call = apiInterface.getWork();
        call.enqueue(new Callback<List<WorkModel>>() {
            @Override
            public void onResponse(Call<List<WorkModel>> call, Response<List<WorkModel>> response) {
                if (response.body() != null) {
                    List<WorkModel> workResponseList = response.body();
                    Log.d("------- workL---------", String.valueOf(workResponseList.size()));
                    listOfWorkTime = new ArrayList<>();

                    workTimeArrayList.clear();
                    workTimeArrayList.add(getString(R.string.work_time));
                    if (workResponseList.size() > 0) {
                        isDataAvailable_workTime = true;
                        for (int i = 0; i < workResponseList.size(); i++) {
                            String workTiming = workResponseList.get(i).getWorkTiming_Name();
                            workTimeArrayList.add(workTiming);

                            listOfWorkTime.add(new MultiSelectModel(i + 1, workTiming));
                        }


                        //MultiSelectModel
                        workTimeMultiSelectDialog = new MultiSelectDialog()
                                .title(getResources().getString(R.string.work_time)) //setting title for dialog
                                .titleSize(25)
                                .positiveText("Done")
                                .negativeText("Cancel")
                                .setMinSelectionLimit(0)
                                .setMaxSelectionLimit(listOfWorkTime.size())
                                //.preSelectIDsList(alreadySelectedCountries) //List of ids that you need to be selected
                                .multiSelectList(listOfWorkTime) // the multi select model list with ids and name
                                .onSubmit(new MultiSelectDialog.SubmitCallbackListener() {
                                    @RequiresApi(api = Build.VERSION_CODES.N)
                                    @Override
                                    public void onSelected(ArrayList<Integer> selectedIds, ArrayList<String> selectedNames, String dataString) {
                                        //will return list of selected IDS
                                        WorkTimingList = new ArrayList<WorkTimingMoel>();
                                        for (int i = 0; i < selectedIds.size(); i++) {
                                           /* Toast.makeText(getActivity().getApplicationContext(), "Selected Ids : "   + selectedIds.get(i) + "\n" + "Selected Names : "   + selectedNames.get(i) + "\n" + "DataString : "
                                                    + dataString, Toast.LENGTH_SHORT).show(); */
                                            workTimingMoel = new WorkTimingMoel();
                                            workTimingMoel.setWorkTiming_Id(selectedIds.get(i));
                                            WorkTimingList.add(i, workTimingMoel);
                                        }

                                        Log.d("***** WorkTimingList***", String.valueOf(WorkTimingList.size()));
                                        workTimeMultiSelectTextView.setText("Work timing count :" + selectedIds.size());
                                        workTimeMultiSelectTextView.clearFocus();
                                        workTimeMultiSelectTextView.setError(null);
                                    }

                                    @Override
                                    public void onCancel() {
                                        Log.d(TAG, "Dialog cancelled");

                                    }
                                });


                        ArrayList<MultiSelectSpinnerModel> listVOs = new ArrayList<>();
                        for (int i = 0; i < workTimeArrayList.size(); i++) {
                            MultiSelectSpinnerModel stateVO = new MultiSelectSpinnerModel();
                            stateVO.setTitle(workTimeArrayList.get(i));
                            stateVO.setSelected(false);
                            listVOs.add(stateVO);
                        }
                        MultiSelectSpinnerAdapter myAdapter = new MultiSelectSpinnerAdapter(getContext(), 0, listVOs);
                        workTimeSpinner.setAdapter(myAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<WorkModel>> call, Throwable t) {
                Log.d("workTimeSpinner_onFail-", t.getMessage());

            }
        });
    }

    private void getLanguage() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<List<LanguageModel>> call = apiInterface.getLanguage();
        call.enqueue(new Callback<List<LanguageModel>>() {
            @Override
            public void onResponse(Call<List<LanguageModel>> call, Response<List<LanguageModel>> response) {
                if (response.body() != null) {
                    List<LanguageModel> languageResponseList = response.body();
                    Log.d("------- lang size----", String.valueOf(languageResponseList.size()));

                    listOfLanguages = new ArrayList<>();

                    languageArrayList.clear();
                    languageArrayList.add(getString(R.string.language));
                    if (languageResponseList.size() > 0) {
                        for (int i = 0; i < languageResponseList.size(); i++) {
                            String languageName = languageResponseList.get(i).getLanguage_Name();
                            languageArrayList.add(languageName);

                            listOfLanguages.add(new MultiSelectModel(i + 1, languageName));
                            Log.d("------- MULTISELECT----", languageName);

                        }

                        //MultiSelectModel
                        multiSelectDialog = new MultiSelectDialog()
                                .title(getResources().getString(R.string.language)) //setting title for dialog
                                .titleSize(25)
                                .positiveText("Done")
                                .negativeText("Cancel")
                                .setMinSelectionLimit(0)
                                .setMaxSelectionLimit(listOfLanguages.size())
                                //.preSelectIDsList(alreadySelectedCountries) //List of ids that you need to be selected
                                .multiSelectList(listOfLanguages) // the multi select model list with ids and name
                                .onSubmit(new MultiSelectDialog.SubmitCallbackListener() {
                                    @RequiresApi(api = Build.VERSION_CODES.N)
                                    @Override
                                    public void onSelected(ArrayList<Integer> selectedIds, ArrayList<String> selectedNames, String dataString) {
                                        //will return list of selected IDS
                                        for (int i = 0; i < selectedIds.size(); i++) {
                                            Toast.makeText(getActivity().getApplicationContext(), "Selected Ids : "
                                                    + selectedIds.get(i) + "\n" + "Selected Names : "
                                                    + selectedNames.get(i) + "\n" + "DataString : "
                                                    + dataString, Toast.LENGTH_SHORT).show();
                                        }


                                        Language_Id_string = new StringBuilder();
                                        for (Integer val : selectedIds) {
                                            Language_Id_string.append(val);
                                            if (selectedIds.size() > 1) {
                                                Language_Id_string.append(",");
                                            }
                                        }
                                        languageMultiSelectTextView.setText("Language count :" + selectedIds.size());
                                        languageMultiSelectTextView.clearFocus();
                                        languageMultiSelectTextView.setError(null);
                                    }

                                    @Override
                                    public void onCancel() {
                                        Log.d(TAG, "Dialog cancelled");

                                    }
                                });


                        ArrayList<MultiSelectSpinnerModel> listVOs = new ArrayList<>();
                        for (int i = 0; i < languageArrayList.size(); i++) {
                            MultiSelectSpinnerModel stateVO = new MultiSelectSpinnerModel();
                            stateVO.setTitle(languageArrayList.get(i));
                            stateVO.setSelected(false);
                            listVOs.add(stateVO);
                        }
                        mylangAdapter = new LanguageMultiSelectSpinnerAdapter(getContext(), 0, listVOs, RegFrag.this);
                        languageSpinner.setAdapter(mylangAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<LanguageModel>> call, Throwable t) {
                Log.d("languageSpinner_onFail-", t.getMessage());

            }
        });
    }

    private Uri GetCropUri() {
        Uri tempUri = null;
        try {
            Calendar cal = Calendar.getInstance();
            cal.setTimeZone(TimeZone.getDefault());
            CropedFile = String.valueOf(cal.getTimeInMillis()) + ".JPG";

            String Folder = Environment.getExternalStorageDirectory() + "/SafayaPhotos/";

            File fileDir = new File(Folder);
            fileDir.mkdir();

            File tempFile = new File(Folder, CropedFile);
            if (tempFile.exists() == false)
                tempFile.createNewFile();

            //  tempUri = Uri.fromFile(tempFile);
            if (Build.VERSION.SDK_INT >= 24) {
                tempUri = FileProvider.getUriForFile(getActivity(), "com.example.safaya", tempFile);
            } else {
                tempUri = Uri.fromFile(tempFile);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tempUri;
    }

    private void selectImageFromGallery(String diffrentiateLayout) {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.putExtra("diffrentiateLayout", diffrentiateLayout);
        startActivityForResult(intent, GALLERY_CODE);
    }

    private void takeImageFromCamera(String diffrentiateLayout) {
        try {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, GetCropUri());
            cameraIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(cameraIntent, CAMERA_CODE);

          /*  Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
            intent.putExtra("diffrentiateLayout", diffrentiateLayout);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(intent, CAMERA_CODE); */

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == CAMERA_CODE) { // camera

                selectedImagePath = Environment.getExternalStorageDirectory() + File.separator + "SafayaPhotos" + File.separator + CropedFile;
                Log.d("Camera selected img****",selectedImagePath);

                if (SelectPhotoFromCameraFlg) {
                    SelectPhoto__CameraImagePath = selectedImagePath;
                    Log.d("SelectPhoto_CAM ImgPath", selectedImagePath);
                    SelectPhotoFromCameraFlg = false;

                    String fileName = selectedImagePath.substring(selectedImagePath.lastIndexOf("/") + 1);
                    selectPhotoTextView.setText(fileName);
                    selectPhotoTextView.clearFocus();
                    selectPhotoTextView.setError(null);

                } else if (DocumentFromCameraFlg) {
                    Document_CameraImagePath = selectedImagePath;
                    Log.d("Document_CAMERAImage", selectedImagePath);
                    DocumentFromCameraFlg = false;

                    selectedDocumentTextView.setVisibility(View.VISIBLE);
                    String fileName = selectedImagePath.substring(selectedImagePath.lastIndexOf("/") + 1);
                    selectedDocumentTextView.setText("Selected document :"+fileName);
                    selectedDocumentTextView.clearFocus();
                    selectedDocumentTextView.setError(null);
                }

             /*   try {
                    Log.d("Base-64 CAMERA img",CommonUtil.ImageToBase64(selectedImagePath));
                } catch (Exception e) {
                    e.printStackTrace();
                } */

            }
            else if (requestCode == GALLERY_CODE) {
              /* Bundle extras=data.getExtras();
               String diffrentiateLayout = extras.getString("diffrentiateLayout");
               Log.d("***diffrentiateLayout",diffrentiateLayout); */

                // gallery
                assert data != null;
                Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getActivity().getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();
                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
                Log.d("path of img from galler", picturePath);

                if (SelectPhotoFromGalFlg) {
                    SelectPhoto_GalleryImagePath = picturePath;
                    Log.d("SelectPhoto_Gal ImgPath", picturePath);
                    SelectPhotoFromGalFlg = false;

                    String fileName = picturePath.substring(picturePath.lastIndexOf("/") + 1);
                    selectPhotoTextView.setText(fileName);
                    selectPhotoTextView.clearFocus();
                    selectPhotoTextView.setError(null);

                } else if (DocumentFromGalFlg) {
                    Document_GalleryImagePath = picturePath;
                    Log.d("Document_GalleryImage", picturePath);
                    DocumentFromGalFlg = false;

                    selectedDocumentTextView.setVisibility(View.VISIBLE);
                    String fileName = picturePath.substring(picturePath.lastIndexOf("/") + 1);
                    selectedDocumentTextView.setText("Selected document :"+fileName);
                    selectedDocumentTextView.clearFocus();
                    selectedDocumentTextView.setError(null);
                }


              /*  if(diffrentiateLayout.trim().equalsIgnoreCase("SelectPhotoLayout")) {
                    SelectPhoto_GalleryImagePath = picturePath;
                }
                else if(diffrentiateLayout.trim().equalsIgnoreCase("DocumentLayout")){
                    Document_GalleryImagePath = picturePath;
                } */

              /*  try {
                    Log.d("Base-64 gal img",CommonUtil.ImageToBase64(picturePath));
                } catch (Exception e) {
                    e.printStackTrace();
                } */

            }
        }

        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                Log.d("Place: " + place.getName() + ", " + place.getId(), "-------");

                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View rowView = inflater.inflate(R.layout.add_button_layout, null);
                parentLinearLayout.setVisibility(View.VISIBLE);
                parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);

                TextView autocomplete_textview_location = rowView.findViewById(R.id.autocomplete_textview_location);
                autocomplete_textview_location.setText(place.getName());

                WorkLocationModel workLocationModelA = new WorkLocationModel();
                workLocationModelA.setWork_Location_Name(place.getName());

                LatLng latlan = place.getLatLng();
                if (latlan != null) {
                    workLocationModelA.setWork_Location_Lat(latlan.latitude);
                    workLocationModelA.setWork_Location_Long(latlan.longitude);
                }

                WorkLocationList.add(workLocationModelA);
                autocomplete_textview_location.setId(View.generateViewId());
                Log.d("---- SET__ID----", String.valueOf(autocomplete_textview_location.getId()));


                ImageView cancelWorkLocationImgBtn = rowView.findViewById(R.id.cancelWorkLocationImgBtn);
                cancelWorkLocationImgBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ((LinearLayout) rowView.getParent()).removeView(rowView);
                        int id = autocomplete_textview_location.getId();
                        Log.d("---- REMOVEDID----", String.valueOf(id));

                        WorkLocationModel workLocationModel2 = new WorkLocationModel();
                        workLocationModel2.setWork_Location_Name(autocomplete_textview_location.getText().toString());
                        Log.d("---- REMOVED_NAME----", autocomplete_textview_location.getText().toString());

                        for (int i = 0; i < WorkLocationList.size(); i++) {
                            if (WorkLocationList.get(i).getWork_Location_Name().trim().equalsIgnoreCase(workLocationModel2.getWork_Location_Name())) {
                                WorkLocationList.remove(i);
                                //WorkLocationList.remove(workLocationModel2);
                            }
                        }

                        Log.d("-----------------------", String.valueOf(WorkLocationList.size()));
                    }
                });

            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.d("places error----", status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
            return;
        }

        if(requestCode == PARENT_AUTOCOMPLETE_REQUEST_CODE)
        {
            if (resultCode == RESULT_OK){
                Place place = Autocomplete.getPlaceFromIntent(data);
                parentWorkLocationTextView.setText(place.getName());
                parentWorkLocationTextView.clearFocus();
                parentWorkLocationTextView.setError(null);


                WorkLocationModel workLocationModel_P = new WorkLocationModel();
                workLocationModel_P.setWork_Location_Name(place.getName());

                LatLng latlan = place.getLatLng();
                if (latlan != null) {
                    workLocationModel_P.setWork_Location_Lat(latlan.latitude);
                    workLocationModel_P.setWork_Location_Long(latlan.longitude);
                }

                WorkLocationList.add(workLocationModel_P);

            }else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.d("Parent places error----", status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
            return;

        }
    }

    private void updateLabel() {
        //   String myFormat = "dd/MM/yyyy"; //In which you need put here
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);

        dateOfBirthTextView.setText(sdf.format(myCalendar.getTime()));
        dateOfBirthTextView.clearFocus();
        dateOfBirthTextView.setError(null);
        DOB_text = sdf.format(myCalendar.getTime());
    }

    public void displaySpinnerData(int count) {
        //  languageSpinner.setLabelFor(count);
      /*  if(!languageArrayList.contains("Selected Items count : ")) {
            languageArrayList.add(0, "Selected Items count : " + count);
        } else{
            languageArrayList.remove(0);
            languageArrayList.add(0,"Selected Items count : "+count);
        } */

        getLanguageV2(count);

   /*   languageArrayList.remove(0);
      languageArrayList.add(0,"Selected Items count : "+count);

      languageArrayList.remove(1);
      languageArrayList.add(1,"Language ");

            ArrayList<MultiSelectSpinnerModel> listVOs = new ArrayList<>();
            for (int i = 0; i < languageArrayList.size(); i++) {
                MultiSelectSpinnerModel stateVO = new MultiSelectSpinnerModel();
                stateVO.setTitle(languageArrayList.get(i));
                stateVO.setSelected(false);
                listVOs.add(stateVO);
            }
            mylangAdapter = new LanguageMultiSelectSpinnerAdapter(getContext(), 0, listVOs,RegFrag.this);
            languageSpinner.setAdapter(mylangAdapter); *
         /*   languageArrayList.add(0,"Selected Items count : "+count);

       ArrayList<MultiSelectSpinnerModel> listVOs = new ArrayList<>();
        for (int i = 0; i < languageArrayList.size(); i++) {
            MultiSelectSpinnerModel stateVO = new MultiSelectSpinnerModel();
            stateVO.setTitle(languageArrayList.get(i));
            stateVO.setSelected(false);
            listVOs.add(stateVO);
        }
        mylangAdapter = new LanguageMultiSelectSpinnerAdapter(getContext(), 0, listVOs,RegFrag.this);
        languageSpinner.setAdapter(mylangAdapter);  */

    }

    public void getLanguageV2(int cnt) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<List<LanguageModel>> call = apiInterface.getLanguage();
        call.enqueue(new Callback<List<LanguageModel>>() {
            @Override
            public void onResponse(Call<List<LanguageModel>> call, Response<List<LanguageModel>> response) {
                if (response.body() != null) {
                    List<LanguageModel> languageResponseList = response.body();
                    languageArrayList.clear();
                    // languageArrayList.add(getString(R.string.language));
                    languageArrayList.add(0, "Language Selected : " + cnt);

                    if (languageResponseList.size() > 0) {

                        for (int i = 0; i < languageResponseList.size(); i++) {
                            String languageName = languageResponseList.get(i).getLanguage_Name();
                            languageArrayList.add(languageName);
                        }

                        ArrayList<MultiSelectSpinnerModel> listVOs = new ArrayList<>();
                        for (int i = 0; i < languageArrayList.size(); i++) {
                            MultiSelectSpinnerModel stateVO = new MultiSelectSpinnerModel();
                            stateVO.setTitle(languageArrayList.get(i));
                            stateVO.setSelected(false);
                            listVOs.add(stateVO);
                        }
                        mylangAdapter = new LanguageMultiSelectSpinnerAdapter(getContext(), 0, listVOs, RegFrag.this);
                        languageSpinner.setAdapter(mylangAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<LanguageModel>> call, Throwable t) {
                Log.d("lang_Spinner_onFailV2-", t.getMessage());

            }
        });
    }

    @Override
    public void getselectedCount(int count) {
        displaySpinnerData(count);
    }

}
