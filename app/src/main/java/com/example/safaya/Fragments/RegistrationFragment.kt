package com.example.safaya.Fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import com.example.safaya.Api.APIClient
import com.example.safaya.Api.APIInterface
import com.example.safaya.Model.EmployeeModel
import com.example.safaya.Model.EmployeeResponseModel
import com.example.safaya.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class RegistrationFragment : Fragment() {

    private var employee_list: List<String>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        val view: View = inflater.inflate(R.layout.fragment_registration, container, false)
       // val languages = resources.getStringArray(R.array.Languages)

        val employeeCodeSpinner = view.findViewById<Spinner>(R.id.employeeCodeSpinner)

      /* val adapter = activity?.let {
            ArrayAdapter<String>(
                it,
                R.layout.support_simple_spinner_dropdown_item,
                languages
            )
        }
        employeeCodeSpinner.adapter = adapter

        employeeCodeSpinner.onItemSelectedListener = object : AdapterView.OnItemClickListener,
            AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(
                adapterView: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                Toast.makeText(
                    activity,
                    adapterView!!.getItemAtPosition(position).toString(),
                    Toast.LENGTH_SHORT
                ).show()

            }

            override fun onItemClick(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                TODO("Not yet implemented")
            }

        }
        */

     //   getEmployee()

        return view
    }


//    private fun getEmployee()
//    {
//        val apiInterface: APIInterface = APIClient.getClient().create(APIInterface::class.java)
//
//        val call: Call<EmployeeResponseModel> = apiInterface.ge()
//
//        call.enqueue(object : Callback<EmployeeResponseModel?>
//        {
//            override fun onResponse(call: Call<EmployeeResponseModel?>, response: Response<EmployeeResponseModel?>)
//            {
//                Log.d("------- RESPONSE ----", response.code().toString())
//                try {
//
//                    val jsonArray : MutableList<EmployeeModel>? = response.body()?.getEmployeeModelList()
//
//                   /* if (jsonArray != null) {
//                        print("-------------Array--------------"+jsonArray)
//
//                       /* for (i in 0 until jsonArray.length()) {
//                            val employee = jsonArray.getJSONObject(i)
//                            employee_list.add(employee.getString("Employee_Name"))
//                        }
//                        val adapter = activity?.let { ArrayAdapter<String>(it, R.layout.support_simple_spinner_dropdown_item, employee_list!!)}
//                        employeeCodeSpinner.adapter = adapter */
//                    } */
//                }catch (e : Exception){
//                    e.printStackTrace()
//                }
//            }
//
//            override fun onFailure(call: Call<EmployeeResponseModel?>, t: Throwable) {
//                Log.d("--- OnFailure----",t.message.toString())
//            }
//        })
//    }

}
