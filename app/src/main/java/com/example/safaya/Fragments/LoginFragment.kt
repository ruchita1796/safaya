package com.example.safaya.Fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.example.safaya.R
import kotlinx.android.synthetic.main.fragment_login.view.*

public class LoginFragment : Fragment() {

    companion object{
        fun newInstance():LoginFragment{
            return LoginFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        val view: View = inflater.inflate(R.layout.fragment_login, container, false)

        view.letsStartButton.setOnClickListener { view ->
            val fm: FragmentManager? = fragmentManager
            val ft: FragmentTransaction = fm!!.beginTransaction()
            ft.replace(R.id.mainActivityFrameLayout, AdminLogin(), "login_fragment_screen")
            ft.commit()
        }
        return view
    }
}
