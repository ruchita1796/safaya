package com.example.safaya.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LoginResponseModel
{
    @SerializedName("EmployeeData")
    @Expose
    private List<LoginModel> loginModelres;

    public List<LoginModel> getLoginModelres() {
        return loginModelres;
    }

    public void setLoginModelres(List<LoginModel> loginModelres) {
        this.loginModelres = loginModelres;
    }
}
