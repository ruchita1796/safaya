package com.example.safaya.Model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class RateModel : Serializable {

    @SerializedName("Rate_Id")
    var Rate_Id: Int? = 0

    @SerializedName("Rate_Name")
    var Rate_Name: String? = null


    @SerializedName("Rate")
    var Rate: String? = null

    @SerializedName("Rate_From")
    var Rate_From: String? = null

    @SerializedName("Rate_To")
    var Rate_To: String? = null

    @SerializedName("Added_Ip")
    var Added_Ip: String? = null

    @SerializedName("Mod_Ip")
    var Mod_Ip: String? = null

    @SerializedName("Added_On")
    var Added_On: String? = null

    @SerializedName("Mod_On")
    var Mod_On: String? = null

    @SerializedName("Status")
    var Status: String? = null

    @SerializedName("Del_Sts")
    var Del_Sts: String? = null

}