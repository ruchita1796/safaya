package com.example.safaya.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class InsertionModel implements Serializable {

    private int City_Id,Experience_Months,Document_Id;

    private String Gender,Name,Contact_Number,Alt_Contact_Number,DOB,Resides_At,Emp_Code,Enrollment_Number;
    private StringBuilder Language_Id;

    private String Photo_File_Name,Document_File_Name;

    private ArrayList<ServiceRateModel> ServiceRateList;

    private ArrayList<WorkTimingMoel> WorkTimingList;

    private ArrayList<WorkLocationModel> WorkLocationList;

    public int getCity_Id() {
        return City_Id;
    }

    public void setCity_Id(int city_Id) {
        City_Id = city_Id;
    }

    public int getExperience_Months() {
        return Experience_Months;
    }

    public void setExperience_Months(int experience_Months) {
        Experience_Months = experience_Months;
    }

    public int getDocument_Id() {
        return Document_Id;
    }

    public void setDocument_Id(int document_Id) {
        Document_Id = document_Id;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getContact_Number() {
        return Contact_Number;
    }

    public void setContact_Number(String contact_Number) {
        Contact_Number = contact_Number;
    }

    public String getAlt_Contact_Number() {
        return Alt_Contact_Number;
    }

    public void setAlt_Contact_Number(String alt_Contact_Number) {
        Alt_Contact_Number = alt_Contact_Number;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getResides_At() {
        return Resides_At;
    }

    public void setResides_At(String resides_At) {
        Resides_At = resides_At;
    }

    public StringBuilder getLanguage_Id() {
        return Language_Id;
    }

    public void setLanguage_Id(StringBuilder language_Id) {
        Language_Id = language_Id;
    }

    public String getPhoto_File_Name() {
        return Photo_File_Name;
    }

    public void setPhoto_File_Name(String photo_File_Name) {
        Photo_File_Name = photo_File_Name;
    }

    public String getDocument_File_Name() {
        return Document_File_Name;
    }

    public void setDocument_File_Name(String document_File_Name) {
        Document_File_Name = document_File_Name;
    }

    public ArrayList<ServiceRateModel> getServiceRateList() {
        return ServiceRateList;
    }

    public void setServiceRateList(ArrayList<ServiceRateModel> serviceRateList) {
        ServiceRateList = serviceRateList;
    }

    public ArrayList<WorkLocationModel> getWorkLocationList() {
        return WorkLocationList;
    }

    public void setWorkLocationList(ArrayList<WorkLocationModel> workLocationList) {
        WorkLocationList = workLocationList;
    }

    public ArrayList<WorkTimingMoel> getWorkTimingList() {
        return WorkTimingList;
    }

    public void setWorkTimingList(ArrayList<WorkTimingMoel> workTimingList) {
        WorkTimingList = workTimingList;
    }

    public String getEmp_Code() {
        return Emp_Code;
    }

    public void setEmp_Code(String emp_Code) {
        Emp_Code = emp_Code;
    }

    public String getEnrollment_Number() {
        return Enrollment_Number;
    }

    public void setEnrollment_Number(String enrollment_Number) {
        Enrollment_Number = enrollment_Number;
    }
}


