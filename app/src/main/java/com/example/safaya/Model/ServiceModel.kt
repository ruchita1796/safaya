package com.example.safaya.Model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ServiceModel : Serializable {

    @SerializedName("Service_Id")
    var Service_Id: Int? = 0

    @SerializedName("Service_Name")
    var Service_Name: String? = null

    @SerializedName("Added_Ip")
    var Added_Ip: String? = null

    @SerializedName("Mod_Ip")
    var Mod_Ip: String? = null

    @SerializedName("Added_On")
    var Added_On: String? = null

    @SerializedName("Mod_On")
    var Mod_On: String? = null

    @SerializedName("Status")
    var Status: String? = null

    @SerializedName("Del_Sts")
    var Del_Sts: String? = null

}
