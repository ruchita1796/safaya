package com.example.safaya.Model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class WorkModel : Serializable {

    @SerializedName("WorkTiming_Id")
    var WorkTiming_Id: Int? = 0

    @SerializedName("WorkTiming_Name")
    var WorkTiming_Name: String? = null

    @SerializedName("WorkTiming_From")
    var WorkTiming_From: Int? = null

    @SerializedName("WorkTiming_To")
    var WorkTiming_To: Int? = null

    @SerializedName("Added_Ip")
    var Added_Ip: String? = null

    @SerializedName("Mod_Ip")
    var Mod_Ip: String? = null

    @SerializedName("Added_On")
    var Added_On: String? = null

    @SerializedName("Mod_On")
    var Mod_On: String? = null

    @SerializedName("Status")
    var Status: String? = null

    @SerializedName("Del_Sts")
    var Del_Sts: String? = null

}
