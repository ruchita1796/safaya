package com.example.safaya.Model;

import java.io.Serializable;

public class WorkLocationModel implements Serializable {

    private String Work_Location_Name;

    private double Work_Location_Lat,Work_Location_Long;
   
    public String getWork_Location_Name() {
        return Work_Location_Name;
    }

    public void setWork_Location_Name(String work_Location_Name) {
        Work_Location_Name = work_Location_Name;
    }

    public double getWork_Location_Lat() {
        return Work_Location_Lat;
    }

    public void setWork_Location_Lat(double work_Location_Lat) {
        Work_Location_Lat = work_Location_Lat;
    }

    public double getWork_Location_Long() {
        return Work_Location_Long;
    }

    public void setWork_Location_Long(double work_Location_Long) {
        Work_Location_Long = work_Location_Long;
    }
}
