package com.example.safaya.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EmployeeResponseModel {

    @SerializedName("error")
    @Expose
    private Boolean error;

    @SerializedName("EmployeeViewModel")
    @Expose
    private List<EmployeeModel> employeeModelList = null;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public List<EmployeeModel> getEmployeeModelList() {
        return employeeModelList;
    }

    public void setEmployeeModelList(List<EmployeeModel> employeeModelList) {
        this.employeeModelList = employeeModelList;
    }

}


