package com.example.safaya.Model;

import java.io.Serializable;

public class ServiceRateModel implements Serializable {

    private int Service_Id,Rate;

    public int getService_Id() {
        return Service_Id;
    }

    public void setService_Id(int service_Id) {
        Service_Id = service_Id;
    }

    public int getRate() {
        return Rate;
    }

    public void setRate(int rate) {
        Rate = rate;
    }
}
