package com.example.safaya.Model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class CityModel : Serializable {

    @SerializedName("City_Id")
    var City_Id: Int? = 0

    @SerializedName("City_Name")
    var City_Name: String? = null

    @SerializedName("Added_Ip")
    var Added_Ip: String? = null

    @SerializedName("Mod_Ip")
    var Mod_Ip: String? = null

    @SerializedName("Added_On")
    var Added_On: String? = null

    @SerializedName("Mod_On")
    var Mod_On: String? = null

    @SerializedName("Status")
    var Status: String? = null

    @SerializedName("Del_Sts")
    var Del_Sts: String? = null

}
