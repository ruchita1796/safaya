package com.example.safaya.Model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class MultiSelectSpinnerModel : Serializable {

    @SerializedName("selected")
    var selected: Boolean? = false

    @SerializedName("title")
    var title: String? = null

}

