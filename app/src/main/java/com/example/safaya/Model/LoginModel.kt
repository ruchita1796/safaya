package com.example.safaya.Model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class LoginModel : Serializable {

    @SerializedName("Employee_Id")
    var Employee_Id: Int? = 0

    @SerializedName("Employee_Name")
    var Employee_Name: String? = null

    @SerializedName("Employee_Code")
    var Employee_Code: String? = null

    @SerializedName("Added_Ip")
    var Added_Ip: String? = null

    @SerializedName("Mod_Ip")
    var Mod_Ip: String? = null

    @SerializedName("Added_On")
    var Added_On: String? = null

    @SerializedName("Mod_On")
    var Mod_On: String? = null

    @SerializedName("Status")
    var Status: String? = null

    @SerializedName("Del_Sts")
    var Del_Sts: String? = null

}
