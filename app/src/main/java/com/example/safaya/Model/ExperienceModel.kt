package com.example.safaya.Model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ExperienceModel : Serializable {

    @SerializedName("Experience_Id")
    var Experience_Id: Int? = 0

    @SerializedName("Experience_Name")
    var Experience_Name: String? = null


    @SerializedName("Experience_From")
    var Experience_From: Int? = 0

    @SerializedName("Experience_To")
    var Experience_To: Int? = 0

    @SerializedName("Added_Ip")
    var Added_Ip: String? = null

    @SerializedName("Mod_Ip")
    var Mod_Ip: String? = null

    @SerializedName("Added_On")
    var Added_On: String? = null

    @SerializedName("Mod_On")
    var Mod_On: String? = null

    @SerializedName("Status")
    var Status: String? = null

    @SerializedName("Del_Sts")
    var Del_Sts: String? = null

}
