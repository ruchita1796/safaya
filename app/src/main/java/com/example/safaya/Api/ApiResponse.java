package com.example.safaya.Api;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


public class ApiResponse implements Serializable {

    public static class ApiError implements Serializable {

        private static final long serialVersionUID = 1L;
        private static final String GEN_ERROR = "Oops, something went wrong.\nPlease try again later.";
        private static final String NET_ERROR = "Oops, something went wrong while communicating with server.\nPlease check your network connection or try again later.";

        private String code;
        private String message;

        public ApiError() {
        }

        public ApiError(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public static final ApiError GENERAL_ERROR = new ApiError(500 + "", GEN_ERROR);
        public static final ApiError NETWORK_ERROR = new ApiError(500 + "", NET_ERROR);

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    private boolean success;
    private ApiError error;
    private Map<String, Object> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }

    public ApiResponse addData(String key, Object value) {
        if (null==data) {
            data = new HashMap<String, Object>();
        }
        data.put(key, value);
        return this;
    }

    public ApiError getError() {
        return error;
    }

    public ApiResponse setError(ApiError error) {
        this.error = error;
        if (error != null) {
            success = false;
        }
        return this;
    }
}
