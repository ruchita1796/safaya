package com.example.safaya.Api;

import com.example.safaya.Model.CityModel;
import com.example.safaya.Model.DocumentModel;
import com.example.safaya.Model.EmployeeModel;
import com.example.safaya.Model.ExperienceModel;
import com.example.safaya.Model.InsertionModel;
import com.example.safaya.Model.LanguageModel;
import com.example.safaya.Model.LoginModel;
import com.example.safaya.Model.LoginResponseModel;
import com.example.safaya.Model.RateModel;
import com.example.safaya.Model.ServiceModel;
import com.example.safaya.Model.WorkModel;
import com.example.safaya.Utils.Constants;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface APIInterface {

    @GET(Constants.GET_EMPLOYEE__URL)
    Call<List<EmployeeModel>> getEmployee();
    //  Call<EmployeeResponseModel> getEmployee();
    // Call<List<EmployeeModel>> getPlaylists();

    @GET(Constants.GET_CITY__URL)
    Call<List<CityModel>> getCity();

    @GET(Constants.GET_EXPERIENCE__URL)
    Call<List<ExperienceModel>> getExperience();

    @GET(Constants.GET_DOCUMENT__URL)
    Call<List<DocumentModel>> getDocument();

    @GET(Constants.GET_SERVICE__URL)
    Call<List<ServiceModel>> getService();

    @GET(Constants.GET_WORK__URL)
    Call<List<WorkModel>> getWork();

    @GET(Constants.GET_LANGUAGE__URL)
    Call<List<LanguageModel>> getLanguage();

    @GET(Constants.GET_RATE__URL)
    Call<List<RateModel>> getRate();

//    @POST(Constants.POST_USER_DATA_URL)
//    Call<InsertionModel> insertUserData(InsertionModel insertionModel);


//    @Multipart
  /*  @POST(Constants.POST_USER_DATA_URL)
    @FormUrlEncoded
    Call<ApiResponse> insertUserData(@Body InsertionModel insertionModel);*/

    @Headers("Content-Type: application/json")
    @POST(Constants.POST_USER_DATA_URL)
    Call<InsertionModel> insertUserData(@Body InsertionModel insertionModel);
//
//    @GET(Constants.LOGIN_URL)
//    Call<LoginModel> Login(@Query("emp_User_Name") String username, @Query("emp_Password") String password);

    @GET(Constants.LOGIN_URL)
    Call<LoginResponseModel> Login(@Query("emp_User_Name") String username, @Query("emp_Password") String password);


}

