package com.example.safaya.Utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.text.TextUtils;
import android.util.Base64;

import androidx.annotation.RequiresApi;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

public class CommonUtil {

    public static boolean isEmpty(String string) {
        return TextUtils.isEmpty(string) || string.contentEquals("null");
    }

    public static boolean IsNull(String input) {
        return input == null || input.equalsIgnoreCase("") || input.trim().length() == 0 || input.trim().contains("null") ;
    }

    public static boolean isValidMobileNumber(String mobileNumber) {

        if(isEmpty(mobileNumber) || !TextUtils.isDigitsOnly(mobileNumber) || mobileNumber.length() < 10)
            return false;

        return true;
    }

    /*
   @RequiresApi(api = Build.VERSION_CODES.O)
   public static String stringToBase64(String imgPath) throws UnsupportedEncodingException {

       byte[] bytes = imgPath.getBytes(StandardCharsets.UTF_8);
       String base64Encoded = Base64.getEncoder().encodeToString(bytes);

       return  base64Encoded;

   } */

    public static String ImageToBase64(String imgPath) throws Exception {

        String sBase64 = "";
        if (!CommonUtil.IsNull(imgPath)) {

            Bitmap temp = null;
            //  Bitmap temp = BitmapFactory.decodeFile(imgPath);
            do {
                temp = BitmapFactory.decodeFile(imgPath);
            } while (temp == null);

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            temp.compress(Bitmap.CompressFormat.JPEG, 50, out);

            byte[] imagebyte = out.toByteArray();
            sBase64 = android.util.Base64.encodeToString(imagebyte, android.util.Base64.DEFAULT);

        }
        return sBase64;
    }

    // not getting image
  /*  public static String encodeImage(String path)
    {
        File imagefile = new File(path);
        FileInputStream fis = null;
        try{
            fis = new FileInputStream(imagefile);
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }
        Bitmap bm = BitmapFactory.decodeStream(fis);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG,100,baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);

        return encImage;

    }
*/
}
