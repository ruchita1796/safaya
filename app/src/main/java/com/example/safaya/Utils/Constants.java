package com.example.safaya.Utils;

public class Constants {

    public static final String BASE_URL = "http://api.safaya.co.in/";

    public static final String GET_EMPLOYEE__URL = BASE_URL + "api/master/GetEmployee";
    public static final String GET_CITY__URL = BASE_URL + "api/master/GetCity";
    public static final String GET_EXPERIENCE__URL = BASE_URL + "api/master/GetExperience";
    public static final String GET_DOCUMENT__URL = BASE_URL + "api/master/GetDocument";
    public static final String GET_SERVICE__URL = BASE_URL + "api/master/GetService";
    public static final String GET_WORK__URL = BASE_URL + "api/master/GetWorkTiming";
    public static final String GET_LANGUAGE__URL = BASE_URL + "api/master/GetLanguage";
    public static final String GET_RATE__URL = BASE_URL + "api/master/GetRate";


    public static final String POST_USER_DATA_URL = BASE_URL + "api/User/InsertFormData";

    public static final String LOGIN_URL = BASE_URL + "api/User/GetAuthenticateEmployee";


}
